﻿using System;

namespace FantasySimulator
{
    public class DefenseCalculator
    {
        public DefenseCalculator()
        {
        }

        public virtual int MinToHit(int baseline, int defense, int attack)
        {
            return Math.Max(Math.Min(baseline - (attack - defense), 20), 1);
        }
    }
}