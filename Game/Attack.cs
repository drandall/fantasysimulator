﻿namespace FantasySimulator
{
    public interface Attack : Ability
    {
        AttackResult UseOn(Combatant enemy, Combatant attacker);
    }
}