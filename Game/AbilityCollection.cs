﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FantasySimulator
{
    public class AbilityCollection
    {
        private List<Ability> abilities = new List<Ability>();

        public void Learn(Ability ability)
        {
            abilities.Add(ability);
        }

        public bool Has<TAbility>()
        {
            return abilities.Any(a => a.GetType() == typeof(TAbility));
        }

        public bool Has(Ability ability)
        {
            var abilityType = ability.GetType();
            return abilities.Any(a => a.GetType() == abilityType);
        }

        public TAbility Get<TAbility>()
        {
            TAbility ability = (TAbility)abilities.SingleOrDefault(a => a.GetType() == typeof(TAbility));

            if (ability == null)
                throw new NotAbleException();

            return ability;
        }
    }
}