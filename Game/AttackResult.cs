﻿namespace FantasySimulator
{
    public interface AttackResult
    {
        int Damage { get; }
    }
}