﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasySimulator
{
    public interface Combatant
    {
        int Dexterity { get; }
        int Constitution { get; }
        int Strength { get; }
        int Health { get; }

        AttackResult Attack<TAttack>(Combatant enemy) where TAttack : Attack;
        void TakeDamage(Hit hit);
    }
}
