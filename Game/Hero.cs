﻿using System;

namespace FantasySimulator
{
    public class Hero : Combatant
    {
        public Hero(Punch punch)
        {
            Abilities = new AbilityCollection();
            Abilities.Learn(punch);
            Strength = 1;
        }

        public AbilityCollection Abilities { get; private set; }
        public int Health { get; private set; }
        public int Dexterity { get; private set; }
        public int Constitution { get; private set; }
        public int Strength { get; private set; }

        public AttackResult Attack<TAttack>(Combatant target) where TAttack : Attack
        {
            return Abilities.Get<TAttack>().UseOn(target, this);
        }

        public void TakeDamage(Hit hit)
        {
            throw new NotImplementedException();
        }
    }
}