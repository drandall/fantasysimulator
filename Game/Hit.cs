﻿namespace FantasySimulator
{
    public class Hit : AttackResult
    {
        public Hit(int damage)
        {
            Damage = damage;
        }

        public int Damage { get; private set; }
    }
}