﻿using System;

namespace FantasySimulator
{
    public class D20
    {
        Random random;

        public D20(Random random)
        {
            this.random = random;
        }

        public virtual int Roll()
        {
            return random.Next(1, 21);
        }
    }
}