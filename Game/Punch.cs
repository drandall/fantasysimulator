﻿using System;

namespace FantasySimulator
{
    public class Punch : Attack
    {
        private D20 die;
        private DefenseCalculator defenseCalculator;

        public Punch(D20 die, DefenseCalculator defenseCalculator)
        {
            this.die = die;
            this.defenseCalculator = defenseCalculator;
        }

        public AttackResult UseOn(Combatant defender, Combatant attacker)
        {
            int roll = die.Roll();

            if (roll != 1 && (roll == 20 || roll >= defenseCalculator.MinToHit(8, defender.Dexterity, attacker.Dexterity)))
            {
                Hit result = new Hit(attacker.Strength - defender.Constitution);
                defender.TakeDamage(result);
                return result;
            }

            return new Miss();
        }
    }
}