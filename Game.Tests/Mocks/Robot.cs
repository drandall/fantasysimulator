﻿using System;
using System.Collections.Generic;

namespace FantasySimulator
{
    internal class Robot : Combatant
    {
        public Robot(int health = 100, int dexterity = 1)
        {
            Health = health;
            Dexterity = dexterity;
        }

        public int Constitution { get; set; }
        public int Dexterity { get; set; }
        public int Health { get; private set; }
        public int Strength { get; set; }

        public AttackResult Attack<TAttack>(Combatant enemy) where TAttack : Attack
        {
            throw new NotAbleException();
        }

        public void LearnAbility(Ability ability)
        {
            throw new NotImplementedException();
        }

        public void TakeDamage(Hit hit)
        {
            Health -= hit.Damage;
        }
    }
}