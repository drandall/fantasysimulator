﻿using System;

namespace FantasySimulator
{
    class FixedD20 : D20
    {
        public FixedD20(int nextRoll = 10)
            : base (new Random())
        {
            NextRoll = nextRoll;
        }

        public int NextRoll { get; internal set; }

        public override int Roll()
        {
            return NextRoll;
        }
    }
}