﻿using System;
using System.Collections.Generic;

namespace FantasySimulator
{
    internal class Dummy : Combatant
    {
        private List<Hit> hits = new List<Hit>();

        public Dummy(int dummyHealth = 1)
        {
            Health = dummyHealth;
            Dexterity = 0;
        }

        public int Constitution { get; set; }
        public int Dexterity { get; set; }
        public int Health { get; set; }
        public int Strength { get; set; }

        public AttackResult Attack<TAttack>(Combatant enemy) where TAttack : Attack
        {
            throw new NotAbleException();
        }

        public void LearnAbility(Ability ability)
        {
            throw new NotImplementedException();
        }

        public void TakeDamage(Hit hit)
        {
            hits.Add(hit);
            Health -= hit.Damage;
        }

        internal bool WasDamagedBy(AttackResult result)
        {
            Hit hit = result as Hit;

            if (hit == null)
                return false;

            return hits.Contains(hit);
        }
    }
}