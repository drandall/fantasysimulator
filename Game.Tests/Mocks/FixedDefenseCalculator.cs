﻿namespace FantasySimulator
{
    public class FixedDefenseCalculator : DefenseCalculator
    {
        public FixedDefenseCalculator(int minToHit = 10)
        {
            FixedValue = minToHit;
        }

        public int FixedValue { get; internal set; }

        public override int MinToHit(int baseline, int defense, int attack)
        {
            LastCalculation = new Calculation(baseline, defense, attack);

            return FixedValue;
        }

        public Calculation LastCalculation { get; private set; }

        public class Calculation
        {
            public Calculation(int baseline, int defense, int attack)
            {
                Defense = defense;
                Baseline = baseline;
                Attack = attack;
            }

            public int Attack { get; internal set; }
            public int Baseline { get; internal set; }
            public int Defense { get; private set; }
        }
    }
}