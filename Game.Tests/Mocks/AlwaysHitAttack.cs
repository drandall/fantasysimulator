﻿using System;
using System.Collections.Generic;

namespace FantasySimulator
{
    internal class AlwaysHitAttack : Attack
    {
        private int damage;
        private HashSet<Combatant> enemiesUsedOn = new HashSet<Combatant>();
        private HashSet<Combatant> attackersUsed = new HashSet<Combatant>();

        public AlwaysHitAttack(int damage)
        {
            this.damage = damage;
        }

        public AttackResult LastResult { get; private set; }

        public AttackResult UseOn(Combatant enemy, Combatant attacker)
        {
            enemiesUsedOn.Add(enemy);
            attackersUsed.Add(attacker);
            LastResult = new Hit(damage);
            return LastResult;
        }

        internal bool WasUsedOn(Combatant target)
        {
            return enemiesUsedOn.Contains(target);
        }

        internal bool WasUsedBy(Combatant attacker)
        {
            return attackersUsed.Contains(attacker);
        }
    }
}