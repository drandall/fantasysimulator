﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace FantasySimulator
{
    [TestClass]
    public class D20Tester
    {
        [TestMethod]
        public void D20_Gets_Next_From_Random()
        {
            var random = new TestRandom();
            var d20 = new D20(random);

            d20.Roll();

            Assert.IsTrue(random.Calls.Count() == 1);
        }

        [TestMethod]
        public void D20_Uses_1_As_MinValue()
        {
            var random = new TestRandom();
            var d20 = new D20(random);

            d20.Roll();

            Assert.AreEqual(1, random.Calls.Single().MinValue);
        }

        [TestMethod]
        public void D20_Uses_21_As_MaxValue()
        {
            var random = new TestRandom();
            var d20 = new D20(random);

            d20.Roll();

            Assert.AreEqual(21, random.Calls.Single().MaxValue);
        }

        public class TestRandom : Random
        {
            List<Call> calls = new List<Call>();

            public IEnumerable<Call> Calls { get { return calls; } }

            public override int Next(int minValue, int maxValue)
            {
                calls.Add(new Call(minValue, maxValue));
                return base.Next(minValue, maxValue);
            }

            public class Call
            {
                public Call(int minValue, int maxValue)
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                }

                public int MinValue { get; private set; }
                public int MaxValue { get; private set; }
            }
        }
    }
}
