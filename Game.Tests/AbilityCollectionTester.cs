﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FantasySimulator
{
    [TestClass]
    public class AbilityCollectionTester
    {
        [TestMethod]
        public void AbilityCollection_Learn_Makes_HasAbility_True()
        {
            var ac = new AbilityCollection();
            var ability = new AlwaysHitAttack(1);

            ac.Learn(ability);

            Assert.IsTrue(ac.Has(ability));
        }

        [TestMethod]
        public void AbilityCollection_Learn_Makes_HasAbility_True_SameType_DiffInstance()
        {
            var ac = new AbilityCollection();
            var ability = new AlwaysHitAttack(1);

            ac.Learn(ability);

            Assert.IsTrue(ac.Has(new AlwaysHitAttack(1)));
        }

        [TestMethod]
        [ExpectedException(typeof(NotAbleException))]
        public void AbilityCollection_Get_ThrowsNotAbleEx_WhenPowerUnknown()
        {
            var ac = new AbilityCollection();

            ac.Get<Punch>();
        }

        [TestMethod]
        public void AbilityCollection_Get_ReturnsPower_WhenLearned()
        {
            var ac = new AbilityCollection();
            ac.Learn(new AlwaysHitAttack(1));

            AlwaysHitAttack ability = ac.Get<AlwaysHitAttack>();

            Assert.IsNotNull(ability);
        }
    }
}
