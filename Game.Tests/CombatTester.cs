﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FantasySimulator
{
    [TestClass]
    public class CombatTester
    {
        [TestMethod]
        public void Combatant_CanPunch_AnotherCombatant()
        {
            Combatant hero = new Hero(new Punch(new D20(new Random()), new DefenseCalculator()));
            var initialDummyHealth = 10;
            Combatant enemy = new Dummy(initialDummyHealth);

            Hit attackResult = hero.Attack<Punch>(enemy) as Hit; // always hit because it's a dummy

            Assert.IsNotNull(attackResult, "Should automatically hit the dummy");
            Assert.IsTrue(attackResult.Damage > 0, "Should deal positive damage");
            Assert.AreEqual(initialDummyHealth - attackResult.Damage, enemy.Health, "Should reduce dummy health");
        }
    }
}
