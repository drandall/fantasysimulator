﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace FantasySimulator
{
    [TestClass]
    public class PunchTester
    {
        [TestMethod]
        public void Punch_AutomaticallyHits_On20()
        {
            var die = new FixedD20();
            die.NextRoll = 20;
            var punch = new Punch(die, new DefenseCalculator());
            var dummy = new Dummy();

            Hit result = punch.UseOn(dummy, new Dummy()) as Hit;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Punch_AutomaticallyMisses_On1()
        {
            var die = new FixedD20();
            die.NextRoll = 1;
            var punch = new Punch(die, new DefenseCalculator());
            var dummy = new Dummy()
            {
                Dexterity = 0
            };
            var robot = new Robot()
            {
                Dexterity = 100
            };

            AttackResult result = punch.UseOn(dummy, robot);

            Assert.IsInstanceOfType(result, typeof(Miss));
        }

        [TestMethod]
        public void Punch_Hits_Above_DefenseCalculator_MinToHit()
        {
            for (int roll = 2; roll <= 19; roll++)
            {
                var die = new FixedD20(roll);
                var defenseCalculator = new FixedDefenseCalculator(10);
                var punch = new Punch(die, defenseCalculator);

                AttackResult result = punch.UseOn(new Robot(), new Robot());

                if (roll < defenseCalculator.FixedValue)
                    Assert.IsInstanceOfType(result, typeof(Miss));
                else
                    Assert.IsInstanceOfType(result, typeof(Hit));
            }
        }

        [TestMethod]
        public void Punch_Sends_DefenderDexterity_ToDefenseCalculator()
        {
            var defenderDexterity = 491739;
            var defender = new Robot(dexterity: defenderDexterity);
            var defenseCalculator = new FixedDefenseCalculator();
            var punch = new Punch(new FixedD20(), defenseCalculator);

            punch.UseOn(defender, new Dummy());

            Assert.AreEqual(defenderDexterity, defenseCalculator.LastCalculation.Defense);
        }

        [TestMethod]
        public void Punch_Sends_AttackerDexterity_ToDefenseCalculator()
        {
            var attackerDexterity = 491736;
            var attacker = new Robot(dexterity: attackerDexterity);
            var defenseCalculator = new FixedDefenseCalculator();
            var punch = new Punch(new FixedD20(), defenseCalculator);

            punch.UseOn(new Dummy(), attacker);

            Assert.AreEqual(attackerDexterity, defenseCalculator.LastCalculation.Attack);
        }

        [TestMethod]
        public void Punch_Sends_BaselineOf8_ToDefenseCalculator()
        {
            var defenseCalculator = new FixedDefenseCalculator();
            var punch = new Punch(new FixedD20(), defenseCalculator);

            punch.UseOn(new Dummy(), new Robot());

            Assert.AreEqual(8, defenseCalculator.LastCalculation.Baseline);
        }

        [TestMethod]
        public void Punch_Deals_Strength_Minus_Constitution_As_Damage()
        {
            var d20 = new FixedD20();
            var defenseCalculator = new FixedDefenseCalculator();
            var punch = new Punch(d20, defenseCalculator);
            var dummy = new Dummy()
            {
                Constitution = 7
            };
            var robot = new Robot()
            {
                Strength = 9
            };

            AttackResult result = punch.UseOn(dummy, robot);

            Assert.AreEqual(robot.Strength - dummy.Constitution, result.Damage);
        }

        [TestMethod]
        public void Punch_Sends_TakeDamage_To_Target()
        {
            var d20 = new FixedD20();
            var defenseCalculator = new FixedDefenseCalculator();
            var punch = new Punch(d20, defenseCalculator);
            var dummy = new Dummy()
            {
                Constitution = 7,
                Health = 10
            };
            var robot = new Robot()
            {
                Strength = 9
            };

            AttackResult result = punch.UseOn(dummy, robot);

            Assert.IsTrue(dummy.WasDamagedBy(result));
        }
    }
}