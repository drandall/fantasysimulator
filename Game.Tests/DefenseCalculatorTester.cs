﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FantasySimulator
{
    [TestClass]
    public class DefenseCalculatorTester
    {
        [TestMethod]
        public void DefenseCalculator_ReducesBaseline_By1_ForEach1_Ahead()
        {
            for (int i = 1; i <= 5; i++)
            {
                var defenseCalculator = new DefenseCalculator();
                var baseline = 10;
                var defense = 1;
                var attack = 1 + i;

                int minToHit = defenseCalculator.MinToHit(baseline, defense, attack);

                Assert.AreEqual(baseline - (attack - defense), minToHit);
            }
        }

        [TestMethod]
        public void DefenseCalculator_IncreasesBaseline_By1_ForEach1_Behind()
        {
            for (int i = 1; i <= 5; i++)
            {
                var defenseCalculator = new DefenseCalculator();
                var baseline = 10;
                var defense = 1 + i;
                var attack = 1;

                int minToHit = defenseCalculator.MinToHit(baseline, defense, attack);

                Assert.AreEqual(baseline + (defense - attack), minToHit);
            }
        }

        [TestMethod]
        public void DefenseCalculator_Capped_At_20()
        {
            var defenseCalculator = new DefenseCalculator();

            int minToHit = defenseCalculator.MinToHit(10, 500, 1);

            Assert.AreEqual(20, minToHit);
        }

        [TestMethod]
        public void DefenseCalculator_BottomsOut_At_1()
        {
            var defenseCalculator = new DefenseCalculator();

            int minToHit = defenseCalculator.MinToHit(10, 1, 500);

            Assert.AreEqual(1, minToHit);
        }
    }
}
