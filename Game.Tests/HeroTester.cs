﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FantasySimulator
{
    [TestClass]
    public class HeroTester
    {
        [TestMethod]
        public void Hero_Initializes_Abilities()
        {
            var hero = new Hero(new Punch(new FixedD20(), new DefenseCalculator()));

            Assert.IsInstanceOfType(hero.Abilities, typeof(AbilityCollection));
        }

        [TestMethod]
        public void Hero_Learns_Punch()
        {
            var punch = new Punch(new FixedD20(), new DefenseCalculator());
            var hero = new Hero(punch);

            Assert.AreEqual(punch, hero.Abilities.Get<Punch>());
        }

        [TestMethod]
        public void Hero_Attack_CallsUse_OnPower_WithTarget()
        {
            var hero = new Hero(new Punch(new FixedD20(), new DefenseCalculator()));
            var attack = new AlwaysHitAttack(1);
            hero.Abilities.Learn(attack);
            var enemy = new Dummy();

            hero.Attack<AlwaysHitAttack>(enemy);

            Assert.IsTrue(attack.WasUsedOn(enemy));
        }

        [TestMethod]
        public void Hero_Attack_CallsUse_OnPower_WithSelf_AsAttacker()
        {
            var hero = new Hero(new Punch(new FixedD20(), new DefenseCalculator()));
            var attack = new AlwaysHitAttack(1);
            hero.Abilities.Learn(attack);
            var enemy = new Dummy();

            hero.Attack<AlwaysHitAttack>(enemy);

            Assert.IsTrue(attack.WasUsedBy(hero));
        }

        [TestMethod]
        [ExpectedException(typeof(NotAbleException))]
        public void Hero_Attack_ThrowsEx_IfHero_DoesNotHave_Ability()
        {
            var hero = new Hero(new Punch(new FixedD20(), new DefenseCalculator()));
            var attack = new AlwaysHitAttack(1);
            var enemy = new Dummy();

            hero.Attack<AlwaysHitAttack>(enemy);
        }

        [TestMethod]
        public void Hero_Attack_ReturnsResult_From_Power()
        {
            var hero = new Hero(new Punch(new FixedD20(), new DefenseCalculator()));
            var attack = new AlwaysHitAttack(1);
            hero.Abilities.Learn(attack);
            var enemy = new Dummy();

            AttackResult result = hero.Attack<AlwaysHitAttack>(enemy);

            Assert.AreEqual(attack.LastResult, result);
        }

        [TestMethod]
        public void Hero_Starts_With_Strength_1()
        {
            var hero = new Hero(new Punch(new D20(new Random()), new DefenseCalculator()));

            Assert.AreEqual(1, hero.Strength);
        }
    }
}
